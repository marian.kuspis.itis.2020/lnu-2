const two = 2;
let oldVal = '';
let newVal = prompt('Enter new value:', oldVal);
if (newVal === '') {
    alert('Invalid value');
} else if (/\s/.test(newVal)) {
    alert('Invalid value');
}else if (newVal) {
    alert(extractMiddle(newVal));
} else {
    // user hit cancel
}
    


function extractMiddle(str) {

    let position;
    let length;

    if(str.length % two === 1) {
        position = str.length / two;
        length = 1;
    } else {
        position = str.length / two - 1;
        length = two;
    }

    return str.substring(position, position + length)
}
